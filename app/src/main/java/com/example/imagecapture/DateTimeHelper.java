package com.example.imagecapture;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Himanshu on 2/10/2018.
 */
public class DateTimeHelper {

    /**
     * Make Sure you check for null before using this static variable
     * or you initialise it before using
     */
    private static SimpleDateFormat simpleDateFormat;

    public static String convertDateTODDMMYYYY(Date dateTimeStamp) {
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        return simpleDateFormat.format(dateTimeStamp);
    }

    public static String convertDateTOYYYYMMDD(Date dateTimeStamp) {
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        return simpleDateFormat.format(dateTimeStamp);
    }

    public static String convertDateToTimeHHMMSSAA(Date dateTimeStamp) {
        simpleDateFormat = new SimpleDateFormat("h:mm a", Locale.US);
        return simpleDateFormat.format(dateTimeStamp);
    }


    /**
     * Converts given date to yyyy-MM-dd (1990-12-20) format
     *
     * @return date in yyyy-MM-dd format or null if given date is not in valid format
     */
    @Nullable
    public static String convertDateTOYYYYMMDD(String dateString) {
        try {

            SimpleDateFormat ddmmyyyyDateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            SimpleDateFormat yyyymmddDateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            Date date = ddmmyyyyDateFormatter.parse(dateString);
            return yyyymmddDateFormatter.format(date);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Converts given date to dd-MM-yyyy (20-12-1990) format
     *
     * @return date in yyyy-MM-dd format or null if given date is not in valid format
     */
    @Nullable
    public static String convertDateTODDMMYYYY(String dateString) {
        try {

            SimpleDateFormat ddmmyyyyDateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            SimpleDateFormat yyyymmddDateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

            Date date = yyyymmddDateFormatter.parse(dateString);
            return ddmmyyyyDateFormatter.format(date);
        } catch (ParseException e) {
            return null;
        }
    }

    //-------------------Current Date Related Methods ------------------------//

    public static String getTodaysDateInDDMMYYYY() {
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        return simpleDateFormat.format(newCalendar.getTime());
    }

    @NonNull
    public static Date getTodaysDate() {
        return new Date();
    }

    public static String getTodaysDateInYYYYMMDD() {
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        return simpleDateFormat.format(newCalendar.getTime());
    }

    public static String getTodaysDateInFormat(String dateFormatString) {
        simpleDateFormat = new SimpleDateFormat(dateFormatString, Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        return simpleDateFormat.format(newCalendar.getTime());
    }


}
