package com.example.imagecapture;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.imagecapture.camera.ImageCallback;
import com.example.imagecapture.camera.ImagesSizes;
import com.example.imagecapture.camera.Result;
import com.example.imagecapture.camera.integrator.CameraIntegrator;
import com.example.imagecapture.camera.integrator.GalleryIntegrator;
import com.example.imagecapture.camera.integrator.Integrator;
import com.example.imagecapture.remote.ApiResponse;
import com.example.imagecapture.remote.ImageApiDao;
import com.example.imagecapture.remote.RestClient;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Base64;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private ImageView capturedImage;
    private static final int PDF_REQUEST_CODE = 20;
    private static final int BUFFER_SIZE = 1024 * 2;

    private TextView choosePdf;
    private AppCompatButton captureImage, uploadImage,uploadPdf;
    private CameraIntegrator cameraIntegrator;
    private GalleryIntegrator galleryIntegrator;
    private String imageClickedPath;
    private ImageApiDao imageApiDao;
    private String selectedPdfPath;
    private ImageCallback cameraResultCallback = new ImageCallback() {
        @Override
        public void onResult(int requestedBy, Result result) {
            imageClickedPath = result.getImagePath();
            Glide.with(MainActivity.this).load(imageClickedPath).apply(RequestOptions.circleCropTransform()).into(capturedImage);

        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        capturedImage=findViewById(R.id.captured_image);
        captureImage=findViewById(R.id.capture_image);
        uploadImage =findViewById(R.id.upload_image);
        uploadPdf =findViewById(R.id.upload_pdf);
        choosePdf =findViewById(R.id.choose_pdf_file);



        setListenerOnViews();

    }

    private void setListenerOnViews()
    {
        choosePdf.setOnClickListener(v -> selectPdfFromGallery());

        uploadPdf.setOnClickListener(view -> {
            if (selectedPdfPath!=null)
                uploadFileOnServer(selectedPdfPath);
            else Toast.makeText(MainActivity.this, "Please select pdf first", Toast.LENGTH_SHORT).show();
        });
        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageClickedPath!=null)
                uploadFileOnServer(imageClickedPath);
                else Toast.makeText(MainActivity.this, "Please click image first", Toast.LENGTH_SHORT).show();
            }
        });

        captureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCameraOptionsDialog();

            }
        });

    }

    private void openGallery() {
        galleryIntegrator = new GalleryIntegrator(this);
        galleryIntegrator.setImageDirectoryName("capturedImages");
        galleryIntegrator.setRequiredImageSize(ImagesSizes.OPTIMUM_SMALL);
        try {
            galleryIntegrator.initiateImagePick();
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }


    private void openCamera() {
        cameraIntegrator = new CameraIntegrator(this);
        cameraIntegrator.setImageDirectoryName("capturedImages");
        cameraIntegrator.setRequiredImageSize(ImagesSizes.OPTIMUM_SMALL);

        try {
            cameraIntegrator.initiateCapture();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void selectPdfFromGallery() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        intent = Intent.createChooser(intent, "Select Pdf File");
        startActivityForResult(intent, PDF_REQUEST_CODE);
    }


    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Integrator.REQUEST_IMAGE_CAPTURE)
            cameraIntegrator.parseResults(requestCode, resultCode, data, cameraResultCallback);
        else if (requestCode == Integrator.REQUEST_IMAGE_PICK)
            galleryIntegrator.parseResults(requestCode, resultCode, data, cameraResultCallback);
        else if (requestCode==PDF_REQUEST_CODE)
        {
            if (data != null && data.getData() != null) {
                // Get the Uri of the selected file
                Uri uri = data.getData();
                if (uri != null) {
                    selectedPdfPath = getFilePathFromURI(MainActivity.this, uri);
                    if (selectedPdfPath.endsWith(".jpg") || selectedPdfPath.endsWith(".gif") || selectedPdfPath.endsWith(".rar") || selectedPdfPath.endsWith(".jpeg") || selectedPdfPath.endsWith(".png") || selectedPdfPath.endsWith(".html") || selectedPdfPath.endsWith(".mp3") || selectedPdfPath.endsWith(".mp4") || selectedPdfPath.endsWith(".zip")) {
                        Toast.makeText(this, "Please select pdf/doc file only", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (selectedPdfPath != null) {
                        choosePdf.setText(selectedPdfPath);
                    }
                }

            }

        }
    }


    public void showCameraOptionsDialog() {
        CharSequence[] image_options = new CharSequence[]{"Open Camera"};

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Select An Option");
        dialog.setIcon(R.drawable.ic_camera_document_image_24dp);
        dialog.setCancelable(true);
        dialog.setItems(image_options, (dialog1, which) -> {
            switch (which) {
                case 0:
                    openCamera();
                    break;
//                case 1:
//                    openGallery();
//                    break;
            }
        });
        dialog.setNegativeButton("Cancel", (dialog12, which) -> dialog12.dismiss());
        dialog.show();
    }





    private void uploadFileOnServer(String imageClickedPath)
    {
        imageApiDao=new RestClient("http://103.130.201.10/").getFileDao();
        File file = new File(imageClickedPath);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        imageApiDao.uploadImageOnServer(body).enqueue(new Callback<ApiResponse<String>>() {
            @Override
            public void onResponse(Call<ApiResponse<String>> call, Response<ApiResponse<String>> response)
            {
                Toast.makeText(MainActivity.this, "success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ApiResponse<String>> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public String getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path
        String fileName = getFileName(contentUri);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + "pdfFiles");
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        if (!TextUtils.isEmpty(fileName)) {
            File copyFile = new File(wallpaperDirectory + File.separator + fileName);
            // create folder if not exists

            copy(context, contentUri, copyFile);
            return copyFile.getAbsolutePath();
        }
        return null;
    }

    public String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }

        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            copystream(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int copystream(InputStream input, OutputStream output) throws Exception, IOException {
        byte[] buffer = new byte[BUFFER_SIZE];

        BufferedInputStream in = new BufferedInputStream(input, BUFFER_SIZE);
        BufferedOutputStream out = new BufferedOutputStream(output, BUFFER_SIZE);
        int count = 0, n = 0;
        try {
            while ((n = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
                out.write(buffer, 0, n);
                count += n;
            }
            out.flush();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
            try {
                in.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
        }

        return count;
    }

    private String convertToBase64(String filePath) {
        String basee4Path = null;
        try {
            File file = new File(filePath);
            byte[] bytes = Files.readAllBytes(file.toPath());
            basee4Path = Base64.getEncoder().encodeToString(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return basee4Path;
    }

    private String encodeFileToBase64Binary(String fileName)
            throws IOException {

        File file = new File(fileName);
        byte[] bytes = loadFile(file);
        String encodedString = android.util.Base64.encodeToString(bytes, android.util.Base64.NO_WRAP);

        return encodedString;
    }

    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > BUFFER_SIZE) {
            // File is too large
        }
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }

}