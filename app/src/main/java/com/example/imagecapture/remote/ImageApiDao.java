package com.example.imagecapture.remote;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ImageApiDao {

    @Multipart
    @POST("imageupload.php")
    Call<ApiResponse<String>>uploadImageOnServer(@Part MultipartBody.Part fileBody);
}
