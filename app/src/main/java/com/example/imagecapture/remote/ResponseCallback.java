package com.example.imagecapture.remote;


import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class

ResponseCallback<T> implements Callback<T> {

    @Override
    public void onResponse(Call<T> call, Response<T> response) {


        switch (response.code()) {

            case HttpURLConnection.HTTP_OK:
            case HttpURLConnection.HTTP_CREATED:
            case HttpURLConnection.HTTP_ACCEPTED:
            case HttpURLConnection.HTTP_NOT_AUTHORITATIVE:

                if (response.body() != null) {
                    onSuccess(response.body());
                } else
                    onError(new Exception(String.format(Locale.getDefault(), "Error, Code: %d Empty Response Body Received, Please try again", response.code())));

                break;

            case HttpURLConnection.HTTP_UNAUTHORIZED:
                onUnauthorised();
                break;

            case HttpURLConnection.HTTP_INTERNAL_ERROR:
            case HttpURLConnection.HTTP_BAD_REQUEST:
                onError(new Exception(String.format("Error, %s ", response.message())));
                break;

            default:
                onError(new Exception(String.format(Locale.getDefault(), "Error, Code : %d ,%s ", response.code(), response.message())));

        }

    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {

        t.printStackTrace();

        if (t instanceof SocketTimeoutException || t instanceof UnknownHostException)
            onError(new Exception("Cannot Connect To Server, Check Internet Connection"));
        else if (t instanceof ConnectException || t instanceof SocketException)
            onError(new Exception("Cannot Connect To Server ,Server Might be Offline"));
        else
            onError(t);
    }


    /**
     * Request executed successfully and we got the response
     *
     * @param response response
     */
    public abstract void onSuccess(@NonNull T response);


    /**
     * Request was classified as unauthorised
     */
    void onUnauthorised() {
        //Make this function abstract if Authorization is implemented
    }

    /**
     * Error Occurred while executing the request
     *
     * @param t
     */
    public abstract void onError(@NonNull Throwable t);
}
