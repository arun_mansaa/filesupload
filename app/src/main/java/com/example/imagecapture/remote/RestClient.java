/*
 * ******************************************************
 *  * Copyright (C) 2018-2019 Himanshu Khati<him.khati@gmail.com>
 *  *
 *  * This file is part of {project}.
 *  *
 *  * {project} can not be copied and/or distributed without the express
 *  * permission of {name}
 *  ******************************************************
 */

package com.example.imagecapture.remote;


import java.util.HashMap;

import retrofit2.Retrofit;


public class RestClient {

    private Retrofit retrofit;
    private WebApiClient webApiClient;

    public RestClient(String baseUrl) {
        webApiClient = new WebApiClient();

        retrofit = webApiClient.getDefaultApiClient(baseUrl);
    }

    public RestClient(Retrofit retrofit) {
        this.retrofit = retrofit;
    }


    public ImageApiDao getFileDao() {
        return retrofit.create(ImageApiDao.class);
    }


}
