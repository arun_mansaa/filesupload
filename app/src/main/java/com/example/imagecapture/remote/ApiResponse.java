package com.example.imagecapture.remote;

import java.util.ArrayList;


/**
  @param <T> Model class name for data which is present in data {@link org.json.JSONArray}
 */
public class ApiResponse<T> {

    private String status;

    private String isError;
    private ArrayList<T> data;

    private String Message = "";

    public String getStatus() {
        return status;
    }

    public String isError() {
        return isError;
    }

    public ArrayList<T> getData() {
        return data;
    }

/*  //  public String getError() {
        return error;
    }*/

    public String getMsg() {
        return Message;
    }
}
