package com.example.imagecapture.remote;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class WebApiClient {

    /**
     * Constants defining timeout intervals
     */
    public final static int CONNECT_TIMEOUT_SECONDS = 55;
    public final static int WRITE_TIMEOUT_SECONDS = 15;
    public final static int READ_TIMEOUT_SECONDS = 20;

    private OkHttpClient okHttpClient;

    public Retrofit getDefaultApiClient(String baseUrl) {
        return getDefaultApiClient(baseUrl, null);
    }

    public Retrofit getDefaultApiClient(@NonNull String baseUrl, Map<String, String> headers) {

        okHttpClient = prepareOkHttpClient(headers, null);
        return getRetrofitWithXmlParser(baseUrl);
    }

    public Retrofit getDefaultApiClient(@NonNull String baseUrl, @Nullable Map<String, String> headers, @Nullable String responseBody) {
        okHttpClient = prepareOkHttpClient(headers, responseBody);
        return getRetrofitClient(baseUrl);
    }


    private Retrofit getRetrofitClient(String baseUrl) {

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();

        return new Retrofit.Builder().client(okHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
    }


    private Retrofit getRetrofitWithXmlParser(String baseUrl) {

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();

        return new Retrofit.Builder().client(okHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
    }

    /**
     * Prepared a custom {@link OkHttpClient} client body for retrofit,that will include @headers by
     * default
     *
     * @param headers header to add in every request
     */
    private OkHttpClient prepareOkHttpClient(@Nullable final Map<String, String> headers, @Nullable final String responseBody) {

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();

        /* ConnectionSpec.MODERN_TLS is the default value */
        List tlsSpecs = Arrays.asList(ConnectionSpec.MODERN_TLS);

        /* providing backwards-compatibility for API lower than Lollipop: */
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            tlsSpecs = Arrays.asList(ConnectionSpec.COMPATIBLE_TLS);
            okHttpBuilder.connectionSpecs(tlsSpecs);
        }
        okHttpBuilder.connectTimeout(CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT_SECONDS, TimeUnit.SECONDS);

      /*  if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpBuilder.addInterceptor(logging);
        }
*/
        okHttpBuilder.addInterceptor(chain -> {
            Request.Builder chainBuilder = chain.request().newBuilder();

            if (headers != null) {
                for (String key : headers.keySet())

                    if (headers.get(key) != null)
                        chainBuilder.addHeader(key, headers.get(key));
            }

            return chain.proceed(chainBuilder.build());
        });

        return okHttpBuilder.build();
    }

}
